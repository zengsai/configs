# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u\[\033[01;33m\]@\[\033[00m\]\[\033[01;32m\]\H\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\H: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
 
alias rmd='rm -rf'

# utran alias
#alias tec='utran -e2c'
#alias tce='utran -c2e'

# for work
alias cdw='cd ~/workspace'
alias cdf='cd ~/software'
alias vimb='vim ~/.bashrc'
alias sourceb='source ~/.bashrc'
alias o='gnome-open'
alias g='global'
alias gb='globash'
alias d='cd'
alias svim='sudo vim'
alias vims='sudo vim'
alias ap='aptitude'
alias sap='sudo aptitude'

# my path
HOME_BIN=~/software/bin
PATH=$HOME_BIN:$PATH

#java
export JAVA_HOME=$HOME/software/jdk
PATH=$JAVA_HOME/bin:$PATH

#android
export ANDROID_HOME=$HOME/software/sdk
PATH=$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH
export USE_CCACHE=1

#android ndk
export NDK=$HOME/software/ndk
PATH=${NDK}:$PATH

# env for golang
export GOROOT=$HOME/sources/go
export GOPATH=$HOME/mygo
#export GOBIN=$GOROOT/bin
export GOARCH=amd64
export GOOS=linux
PATH=$GOPATH/bin:$GOROOT/bin:$HOME/bin:$PATH
alias cdgo='cd $GOPATH'
alias cdgr='cd $GOROOT'
#alias cdsai='cd $GOPATH/src/sai.xmimx.org'
alias cdapb='cd $HOME/mygo/src/git.oschina.net/zengsai/apb'
alias goweb='godoc -http=:6060 -index -index_files="/home/sai/.goindex" -play -src'


#export RUST_HOME=$HOME/software/rust
#PATH=$RUST_HOME/bin:$PATH

PATH=`echo $PATH | tr ":" "\n" | grep -v "^$" | awk '!($0 in a){a[$0];print}' | tr "\n" ":"`


# alias for rust
alias rr='rust run'
alias rt='rust test'
alias rc='rustc'

#alias go='6g'
#alias gl='6l'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi
