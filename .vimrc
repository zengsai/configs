execute pathogen#infect()

let mapleader=";"

im <leader>e <esc>
cm <leader>e <C-C>
nm <leader>w :w<cr>
im <leader>w <esc>:w<cr>

"为窗口跳转重新定义键映射
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l


map <C-i> <C-W>_

imap <leader>4 {<cr>}<Esc>O
imap <c-e> <esc>A
imap <leader>f <right>
imap <leader>b <left>
nmap <space> <c-f>

set nu
set autoread         "当正在编辑的文档在Vim之外被修改后自动加载
set incsearch        "增量搜索
set nocompatible
set nobackup
set nowritebackup
set hidden

" tab setting
set tabstop=4        "设定tab宽度为4个字符
set shiftwidth=4     "设定自动缩进为4个字符
set expandtab        "用space替代tab的输入

syntax on
filetype on
filetype plugin on
filetype indent on

set encoding=utf-8
set fileencoding=utf-8
if has("multi_byte")
    set fileencodings=ucs-bom,utf-8,chinese
    set ambiwidth=double 
endif

" for tagbar
nmap <leader>t :TagbarToggle<cr>
let g:tagbar_left = 1

" for Lusty Explorer
nmap <silent> <Leader>f :LustyFilesystemExplorer<CR>
nmap <silent> <Leader>d :LustyFilesystemExplorerFromHere<CR>
nmap <silent> <Leader>b :LustyBufferExplorer<CR>
nmap <silent> <Leader>g :LustyBufferGrep<CR>
